import unittest
from BSTestRunner import BSTestRunner
import time,logging
import sys
path='D:\\BAAS_TEST\\'
sys.path.append(path)

test_dir='../businessView'
report_dir='../reports'

discover=unittest.defaultTestLoader.discover(test_dir,pattern='test_login.py')

now=time.strftime('%Y-%m-%d %H_%M_%S')
report_name=report_dir+'/'+now+' test_report.html'

with open(report_name,'wb') as f:
    runner=BSTestRunner(stream=f,title='baas Test Report',description='baas Android app test report')
    logging.info('start run test case...')
    runner.run(discover)