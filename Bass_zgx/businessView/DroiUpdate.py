import logging
from common.common_fun import Common,NoSuchElementException
from common.desired_caps import appium_desired
from selenium.webdriver.common.by import By
from time import sleep

class BaasUpdate(Common):
    #应用更新com.droi.sdk.selfupdatedemo:id/silent_update
    gengxin_1 = (By.ID, 'com.droi.sdk.selfupdatedemo:id/normal_ui_upate')#静默ul
    gengxin_2 = (By.ID, 'com.droi.sdk.selfupdatedemo:id/custom_ui_update')#自定义ul
    gengxin_3 = (By.ID, 'com.droi.sdk.selfupdatedemo:id/silent_update')#静默
    gengxin_4 =(By.ID,'com.droi.sdk.selfupdatedemo:id/force_upadte')#强制
    gengxin_5 = (By.ID, 'com.droi.sdk.selfupdatedemo:id/manual_update')#手动
    gengxin_dl = (By.ID, 'com.droi.sdk.selfupdatedemo:id/delet_update_file')#删除
    #应用内文件更新
    file_ul =(By.ID,'com.droi.sdk.selfupdatedemo:id/inapp_update')
    file_dl =(By.ID,'com.droi.sdk.selfupdatedemo:id/delete_inapp_update_file')
    file_dw =(By.ID,'com.droi.sdk.selfupdatedemo:id/download_inapp_update')

    ljgx = (By.ID, 'com.droi.sdk.selfupdatedemo:id/droi_update_id_ok')  # 点击立即更新
    qu_ding = (By.ID, 'android:id/button1')
    quxiao = (By.CLASS_NAME, 'android.widget.Button')  # 点击取消安装
    def DroiUpdate(self):

        #应用更新
        self.driver.find_element(*self.gengxin_1).click()#进入
        logging.info("====静默ul更新====")
        sleep(2)
        try:
            update = self.driver.find_element(*self.ljgx)
        except NoSuchElementException:
            logging.error('login Fail!')
            # self.getScreenShot('login fail')
            return False
        else:
            logging.info('login success!')
            update.click()
            self.driver.find_element(*self.qu_ding).click()
            self.driver.find_elements(*self.quxiao)[1].click()
            return True
        # self.anzhong()

        # self.driver.find_element(*self.gengxin_2).click()  # 进入
        # logging.info("====自定义ul更新====")
        # # sleep(2)
        # # #self.anzhong()
        #
        # self.driver.find_element(*self.gengxin_3).click()  # 进入
        # logging.info("====静默更新====")
        # sleep(2)
        # # self.anzhong()
        # #
        # self.driver.find_element(*self.gengxin_4).click()  # 进入
        # logging.info("====强制更新====")
        # sleep(2)
        # try:
        #     update = self.driver.find_element(*self.ljgx)
        # except NoSuchElementException:
        #     logging.error('login Fail!')
        #     # self.getScreenShot('login fail')
        #     return False
        # else:
        #     logging.info('login success!')
        #     update.click()
        #     self.driver.find_element(*self.quxiao).click()
        #     return True
        # # self.anzhong()
        # #
        # self.driver.find_element(*self.gengxin_5).click()  # 进入
        # logging.info("====手动更新====")
        # sleep(2)
        # try:
        #     update = self.driver.find_element(*self.ljgx)
        # except NoSuchElementException:
        #     logging.error('login Fail!')
        #     # self.getScreenShot('login fail')
        #     return False
        # else:
        #     logging.info('login success!')
        #     update.click()
        #     self.driver.find_element(*self.quxiao).click()
        #     return True
        #
        # # self.anzhong()
        # sleep(2)
        # self.driver.find_element(*self.gengxin_dl).click()  # 进入
        # logging.info("====删除更新下载文件====")
        # sleep(1)
        # self.getScreenShot('删除更新下载文件')
        #
        #
        # # #应用内更新
        # self.driver.find_element(*self.file_ul).click()  # 进入
        # logging.info("====应用内更新====")
        # self.getScreenShot('应用内更新')
        # self.driver.find_element(*self.file_dl).click()  # 进入
        # logging.info("====删除文件====")
        # self.getScreenShot('删除文件')
        # self.driver.find_element(*self.file_dw).click()  # 进入
        # logging.info("====下载文件====")
        # self.getScreenShot('下载文件')

if __name__ == '__main__':
    driver=appium_desired()
    l=BaasUpdate(driver)
    l.DroiUpdate()


