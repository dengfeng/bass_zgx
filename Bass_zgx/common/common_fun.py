from baseView.baseView import BaseView
from common.desired_caps import appium_desired
from selenium.common.exceptions import NoSuchElementException
import logging
from selenium.webdriver.common.by import By
import time,os
import csv

class Common(BaseView):
    ljgx = (By.ID, 'com.droi.sdk.selfupdatedemo:id/droi_update_id_ok')#点击立即更新
    qu_ding=(By.ID,'android:id/button1')
    quxiao = (By.CLASS_NAME, 'android.widget.Button')#点击取消安装
    def anzhong(self):
        try:
            update = self.driver.find_element(*self.ljgx)
        except NoSuchElementException:
            logging.error('login Fail!')
            # self.getScreenShot('login fail')
            return False
        else:
            logging.info('login success!')
            update.click()
            self.driver.find_element(*self.qu_ding).click()
            self.driver.find_elements(*self.quxiao)[1].click()
            return True


    def get_size(self):
        x = self.driver.get_window_size()['width']
        y = self.driver.get_window_size()['height']
        return x, y

    def swipetop(self):
        logging.info('swipetop')
        l = self.get_size()
        y1 = int(l[1] * 0.9)
        x1 = int(l[0] * 0.5)
        y2 = int(l[1] * 0.1)
        self.swipe(x1, y1, x1, y2, 1000)

    def getTime(self):
        self.now=time.strftime("%Y-%m-%d %H_%M_%S")
        return self.now

    def getScreenShot(self,module):
        time=self.getTime()
        image_file=os.path.dirname(os.path.dirname(__file__))+'/screenshots/%s_%s.png'%(module,time)

        logging.info('get %s screenshot'% module)
        self.driver.get_screenshot_as_file(image_file)

    def get_csv_data(self,csv_file,line):
        logging.info('=====get_csv_data======')
        with open(csv_file,'r',encoding='utf-8-sig') as file:
            reader=csv.reader(file)
            for index,row in enumerate(reader,1):
                if index==line:
                    return row

    csv_file = '../data/account.csv'

if __name__ == '__main__':
    driver=appium_desired()
    com=Common(driver)
    com.anzhong()
    #com.swipetop()
    # com.getScreenShot('startApp')










